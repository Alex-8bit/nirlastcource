/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package climatereferencebook;


public class DescriptionFile {

    private int stationID;
    private String nameStation;
    private double[] indicators = new double[12];
   
    private int year;

    public int getStationID() {
        return stationID;
    }

    public void setStationID(int stationID) {
        this.stationID = stationID;
    }

    public String getNameStation() {
        return nameStation;
    }

    public void setNameStation(String nameStation) {
        this.nameStation = nameStation;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double[] getIndicators() {
        return indicators;
    }

    public void setIndicators(double[] indicators) {
        this.indicators = indicators;
    }

   

}
