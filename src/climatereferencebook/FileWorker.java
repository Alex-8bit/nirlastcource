/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package climatereferencebook;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FileWorker {

    public ArrayList<DescriptionFile> getDf() {
        return df;
    }

    public void setDf(ArrayList<DescriptionFile> df) {
        this.df = df;
    }
    public static ArrayList<DescriptionFile> df = new ArrayList<DescriptionFile>();

    public void readFile(String name) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(name));
            Map<Integer, String> sp = readSpavkaName("C:\\Users\\User\\Desktop\\Alexey\\Алексей\\T_Cat.txt");
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] arr = strLine.trim().split("\\s+");
                DescriptionFile obj = new DescriptionFile();
                int st1 = Integer.parseInt(arr[0]);
                obj.setStationID(st1);
                obj.setNameStation(sp.get(st1));
                double[] dd = obj.getIndicators();
                for (int i = 0; i < 12; ++i) {
                    dd[i] = Double.parseDouble(arr[1 + i]);
                }
                // obj.setYear(Integer.parseInt(arr[14])); 
                df.add(obj);
               
                
            }
            System.out.println();
            br.close();
        } catch (Exception ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        TableView f = new TableView();
        f.setVisible(true);
        graphic g = new graphic();
        g.setVisible(true);
    }

    public Map<Integer, String> readSpavkaName(String name) {

        BufferedReader br = null;
        Map<Integer, String> sp = null;
        try {
            sp = new HashMap<Integer, String>();
            br = new BufferedReader(new FileReader(name));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] arr = strLine.replaceAll("﻿", "").split("\\s+");
                //System.out.println(("sdsd:" + arr[0])); 
                // arr[0]=arr[0].replace("﻿", ""); 
                sp.put(Integer.parseInt(arr[0]), arr[1]);
                // System.out.println(arr[0]); 
            }
        } catch (Exception ex) {
            Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(FileWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return sp;
    }

    public static void main(String[] args) {
        FileWorker fw = new FileWorker();
        fw.readFile("C:\\Users\\User\\Desktop\\Alexey\\Алексей\\T_01_02_05.dat");
 
    }

}
